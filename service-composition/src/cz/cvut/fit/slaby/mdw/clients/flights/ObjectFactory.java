
package cz.cvut.fit.slaby.mdw.clients.flights;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cz.cvut.fit.slaby.mdw.flights.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteBooking_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "deleteBooking");
    private final static QName _ListBookingsResponse_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "listBookingsResponse");
    private final static QName _AddBooking_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "addBooking");
    private final static QName _DeleteBookingResponse_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "deleteBookingResponse");
    private final static QName _UpdateBooking_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "updateBooking");
    private final static QName _UpdateBookingResponse_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "updateBookingResponse");
    private final static QName _AddBookingResponse_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "addBookingResponse");
    private final static QName _ListBookings_QNAME = new QName("http://service.flights.mdw.slaby.fit.cvut.cz/", "listBookings");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cz.cvut.fit.slaby.mdw.flights.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddBooking }
     * 
     */
    public AddBooking createAddBooking() {
        return new AddBooking();
    }

    /**
     * Create an instance of {@link DeleteBookingResponse }
     * 
     */
    public DeleteBookingResponse createDeleteBookingResponse() {
        return new DeleteBookingResponse();
    }

    /**
     * Create an instance of {@link UpdateBooking }
     * 
     */
    public UpdateBooking createUpdateBooking() {
        return new UpdateBooking();
    }

    /**
     * Create an instance of {@link UpdateBookingResponse }
     * 
     */
    public UpdateBookingResponse createUpdateBookingResponse() {
        return new UpdateBookingResponse();
    }

    /**
     * Create an instance of {@link AddBookingResponse }
     * 
     */
    public AddBookingResponse createAddBookingResponse() {
        return new AddBookingResponse();
    }

    /**
     * Create an instance of {@link ListBookings }
     * 
     */
    public ListBookings createListBookings() {
        return new ListBookings();
    }

    /**
     * Create an instance of {@link DeleteBooking }
     * 
     */
    public DeleteBooking createDeleteBooking() {
        return new DeleteBooking();
    }

    /**
     * Create an instance of {@link ListBookingsResponse }
     * 
     */
    public ListBookingsResponse createListBookingsResponse() {
        return new ListBookingsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "deleteBooking")
    public JAXBElement<DeleteBooking> createDeleteBooking(DeleteBooking value) {
        return new JAXBElement<DeleteBooking>(_DeleteBooking_QNAME, DeleteBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListBookingsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "listBookingsResponse")
    public JAXBElement<ListBookingsResponse> createListBookingsResponse(ListBookingsResponse value) {
        return new JAXBElement<ListBookingsResponse>(_ListBookingsResponse_QNAME, ListBookingsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "addBooking")
    public JAXBElement<AddBooking> createAddBooking(AddBooking value) {
        return new JAXBElement<AddBooking>(_AddBooking_QNAME, AddBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "deleteBookingResponse")
    public JAXBElement<DeleteBookingResponse> createDeleteBookingResponse(DeleteBookingResponse value) {
        return new JAXBElement<DeleteBookingResponse>(_DeleteBookingResponse_QNAME, DeleteBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBooking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "updateBooking")
    public JAXBElement<UpdateBooking> createUpdateBooking(UpdateBooking value) {
        return new JAXBElement<UpdateBooking>(_UpdateBooking_QNAME, UpdateBooking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "updateBookingResponse")
    public JAXBElement<UpdateBookingResponse> createUpdateBookingResponse(UpdateBookingResponse value) {
        return new JAXBElement<UpdateBookingResponse>(_UpdateBookingResponse_QNAME, UpdateBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddBookingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "addBookingResponse")
    public JAXBElement<AddBookingResponse> createAddBookingResponse(AddBookingResponse value) {
        return new JAXBElement<AddBookingResponse>(_AddBookingResponse_QNAME, AddBookingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListBookings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.flights.mdw.slaby.fit.cvut.cz/", name = "listBookings")
    public JAXBElement<ListBookings> createListBookings(ListBookings value) {
        return new JAXBElement<ListBookings>(_ListBookings_QNAME, ListBookings.class, null, value);
    }

}
