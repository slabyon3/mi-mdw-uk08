package cz.cvut.fit.slaby.mdw.composition.service;

import cz.cvut.fit.slaby.mdw.clients.flights.FlightBooking;
import cz.cvut.fit.slaby.mdw.clients.flights.FlightBookingService;
import cz.cvut.fit.slaby.mdw.clients.trips.TripBooking;
import cz.cvut.fit.slaby.mdw.clients.trips.TripBookingService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@WebService
public class GlobalBooking {
    static FlightBooking flightBooking;
    static TripBooking tripBooking;

    @WebMethod
    public String makeBooking(@WebParam(name = "type") String type,
                              @WebParam(name = "name") String name,
                              @WebParam(name = "destination") String destination) {
        switch (type) {
            case "trip":
                return makeTripBooking(name, destination);
            case "flight":
                return makeFlightBooking(name, destination);
            default:
                return "Unknown booking type!";
        }
    }

    @WebMethod
    public String updateBooking(@WebParam(name = "type") String type,
                              @WebParam(name = "bookingId") int id,
                              @WebParam(name = "name") String name) {
        switch (type) {
            case "trip":
                return updateTripBooking(id, name);
            case "flight":
                return updateFlightBooking(id, name);
            default:
                return "Unknown booking type!";
        }
    }

    private String updateFlightBooking(int id, String name) {
        return getFlightBookingService().updateBooking(id, name);
    }

    private String updateTripBooking(int id, String name) {
        return getTripBookingService().updateBooking(id, name);
    }

    @WebMethod
    public String deleteBooking(@WebParam(name = "type") String type,
                              @WebParam(name = "bookingId") int id) {
        switch (type) {
            case "trip":
                return deleteTripBooking(id);
            case "flight":
                return deleteFlightBooking(id);
            default:
                return "Unknown booking type!";
        }
    }

    private String deleteFlightBooking(int id) {
        return getFlightBookingService().deleteBooking(id);
    }

    private String deleteTripBooking(int id) {
        return getTripBookingService().deleteBooking(id);
    }

    @WebMethod
    public List<String> listBookings(@WebParam(name = "type") String type) {
        switch (type) {
            case "trip":
                return listTripBooking();
            case "flight":
                return listFlightBooking();
            default:
                return null;
        }
    }

    private List<String> listFlightBooking() {
        return getFlightBookingService().listBookings();
    }

    private List<String> listTripBooking() {
        return getTripBookingService().listBookings();
    }

    private FlightBooking getFlightBookingService() {
        if (flightBooking == null)
            flightBooking = new FlightBookingService().getFlightBookingPort();
        return flightBooking;
    }

    private TripBooking getTripBookingService() {
        if (tripBooking == null)
            tripBooking = new TripBookingService().getTripBookingPort();
        return tripBooking;
    }

    private String makeFlightBooking(String name, String destination) {
        return getFlightBookingService().addBooking(name, destination);
    }

    private String makeTripBooking(String name, String destination) {
        return getTripBookingService().addBooking(name, destination);
    }
}
