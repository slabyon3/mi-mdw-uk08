package cz.cvut.fit.slaby.mdw.trips;

import java.util.*;

public class TripDB {
    private static TripDB instance;

    private Map<Trip, List<Booking>> trips;
    private Map<Integer, Booking> bookings;

    public static TripDB getInstance() {
        if (instance == null) instance = new TripDB();
        return instance;
    }

    public TripDB() {
        trips = new HashMap<>();
        bookings = new HashMap<>();

        trips.put(
                new Trip("Budihostice", 10),
                new ArrayList<>()
        );
        trips.put(
                new Trip("Krkonose", 25),
                new ArrayList<>()
        );
    }

    public Trip findTrip(String destintion) {
        for (Trip t : trips.keySet()) {
            if (t.getName().equals(destintion)) {
                return t;
            }
        }
        return null;
    }

    public void addBooking(Booking booking) {
        bookings.put(booking.getId(), booking);
        trips.get(booking.getTrip()).add(booking);
    }

    public void removeBooking(Booking booking) {
        bookings.remove(booking.getId());
        trips.get(booking.getTrip()).remove(booking);
    }

    public Collection<Booking> getAllBookings() {
        return bookings.values();
    }

    public Booking findBooking(int bookingId) {
        return bookings.getOrDefault(bookingId, null);
    }
}
