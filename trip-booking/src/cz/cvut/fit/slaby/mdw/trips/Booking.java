package cz.cvut.fit.slaby.mdw.trips;

import java.io.Serializable;

public class Booking implements Serializable {
    private static int nextId = 0;
    private int id;
    private String name;
    private Trip trip;

    public Booking(String name, Trip trip) {
        this.name = name;
        this.trip = trip;
        this.id = nextId++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "booking id: " +
                String.valueOf(id) +
                " for " +
                trip.toString() +
                " for name: " +
                name.toString();
    }
}
