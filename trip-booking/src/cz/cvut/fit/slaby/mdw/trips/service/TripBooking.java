package cz.cvut.fit.slaby.mdw.trips.service;

import cz.cvut.fit.slaby.mdw.trips.Booking;
import cz.cvut.fit.slaby.mdw.trips.Trip;
import cz.cvut.fit.slaby.mdw.trips.TripDB;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.awt.print.Book;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@WebService
public class TripBooking {
    @WebMethod
    public String addBooking(@WebParam(name = "name") String name,
                             @WebParam(name = "destination") String destination) {
        TripDB db = TripDB.getInstance();

        Trip trip = db.findTrip(destination);
        if (trip == null) {
            return "No such trip found!";
        } else {
            Booking booking = new Booking(name, trip);
            db.addBooking(booking);
            return booking.toString();
        }
    }

    @WebMethod
    public List<String> listBookings() {
        TripDB db = TripDB.getInstance();
        Collection<Booking> bookings = db.getAllBookings();

        List<String> result = new ArrayList<>();
        for (Booking booking : bookings) {
            result.add(booking.toString());
        }
        return result;
    }

    @WebMethod
    public String updateBooking(@WebParam(name = "bookingId") int bookingId,
                                @WebParam(name = "newName") String newName) {
        TripDB db = TripDB.getInstance();

        Booking booking = db.findBooking(bookingId);
        if (booking == null) {
            return "No such booking found!";
        } else {
            booking.setName(newName);
            return booking.toString();
        }
    }

    @WebMethod
    public String deleteBooking(@WebParam(name = "bookingId") int bookingId) {
        TripDB db = TripDB.getInstance();

        Booking booking = db.findBooking(bookingId);
        if (booking == null) {
            return "No such booking found!";
        } else {
            db.removeBooking(booking);
            return booking.toString();
        }
    }
}
