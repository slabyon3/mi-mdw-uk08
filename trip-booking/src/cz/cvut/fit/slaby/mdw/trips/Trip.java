package cz.cvut.fit.slaby.mdw.trips;

import java.io.Serializable;

public class Trip implements Serializable {
    private String name;
    private int capacity;

    public Trip(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "trip to: " +
                String.valueOf(name) +
                " with capacity: " +
                String.valueOf(capacity);
    }
}
